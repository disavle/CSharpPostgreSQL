# CSharpPostgreSQL
Study project Windows Forms with PostgreSQL

# Task: 
Клиенты формируют заказы в онлайн магазине, в которых указываются заказанные товары, их количество и сумма (экспорт в MS Excel). Если заказа подтверждается магазином, то клиент должен его оплатить в течение 20 дней. После оплаты заказ может быть доставлен покупателю. При получении товаров заказ переносится в список исполненных, а просроченные или отказанные – в список аннулированных. На дату для указанных товаров сформировать отчет по клиентам их заказывающих, с указанием количества и суммы доставленных и аннулированных заказов.  За период сформировать отчет по клиентам, соответствующую суммам доставленных заказов.

# Usage:
1. Download PostreSQL - https://www.postgresql.org/download/
2. Install package and run the server.
3. Change connection to the server in a code as well as in installation.
4. Good job!)

# Proceccing:
1. Select client and add items to list
<img width="854" alt="Screenshot 2022-05-03 at 8 49 35 PM" src="https://user-images.githubusercontent.com/69400943/166518936-16201f76-4515-4f2a-88b1-edda12e35f13.png">
2. Create new order
<img width="1088" alt="Screenshot 2022-05-03 at 8 49 42 PM" src="https://user-images.githubusercontent.com/69400943/166519003-9a2288c3-aa57-4ec7-937b-8cfba8edcfcd.png">
3. Go to tab "Orders" 
<img width="862" alt="Screenshot 2022-05-03 at 8 49 50 PM" src="https://user-images.githubusercontent.com/69400943/166519112-eac99b2a-0788-4be8-893e-566007b432cc.png">
4. Select order by double click
<img width="1201" alt="Screenshot 2022-05-03 at 8 50 04 PM" src="https://user-images.githubusercontent.com/69400943/166519170-7d194f19-a065-4e26-bffd-ab28fca5b8fd.png">
5. Edit order, namely: delete one item, check the order is paid and dilivered
<img width="1106" alt="Screenshot 2022-05-03 at 8 50 42 PM" src="https://user-images.githubusercontent.com/69400943/166519357-d0499da1-6ea7-4185-8f31-b67487b0547f.png">
6. Save changes
<img width="1088" alt="Screenshot 2022-05-03 at 8 50 49 PM" src="https://user-images.githubusercontent.com/69400943/166519422-542ea46f-f548-4775-87b4-d5dbc1b923c0.png">
7. The order trasfered to "Delivered"
<img width="853" alt="Screenshot 2022-05-03 at 8 50 57 PM" src="https://user-images.githubusercontent.com/69400943/166519511-58b5db84-a1a2-41f8-8034-26325986922b.png">
8. In this state order can not be edited or canceled
<img width="1104" alt="Screenshot 2022-05-03 at 8 51 03 PM" src="https://user-images.githubusercontent.com/69400943/166519602-8b57277a-078e-4c49-a9af-502f99344975.png">
9. Go to tab "Report", select dates and generate report
<img width="859" alt="Screenshot 2022-05-03 at 8 51 15 PM" src="https://user-images.githubusercontent.com/69400943/166519704-d77a57a4-0086-4236-862a-4ecf5c3fa347.png">
10. Open a report in Excel
<img width="1407" alt="Screenshot 2022-05-03 at 8 52 14 PM" src="https://user-images.githubusercontent.com/69400943/166519861-f7c5dba9-8200-4eb4-87b9-ddb270b88eda.png">


