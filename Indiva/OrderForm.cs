﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;
/* using Excel = Microsoft.Office.Interop.Excel;*/

namespace Indiva
{
    public partial class OrderForm : Form
    {
        private NpgsqlConnection con;
        private string connString = "Host=127.0.0.1;Username=postgres;Password=admin;Database=postgres";
        public OrderForm()
        {
            InitializeComponent();
            con = new NpgsqlConnection(connString);
            con.Open();
           

        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.dataGridView1.SelectedRows)
            {
                dataGridView1.Rows.RemoveAt(item.Index);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string sql2 = "update orders set orderState = 5 where num ='" + label11.Text+"'";
            NpgsqlCommand cmd = new NpgsqlCommand(sql2, con);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
            MessageBox.Show("Заказ №" + label11.Text + " успешно отменен.");

            Close();



        }

        private void OrderForm_FormClosed(object sender, FormClosedEventArgs e)
        {

            con.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // изменение клиента по заказу

            string sql1 = "select client_id from orders where num = '" + label11.Text + "'";
            NpgsqlCommand com = new NpgsqlCommand(sql1, con);
            string id = com.ExecuteScalar().ToString();

            string sql2 = "update clients set name = '"+textBox1.Text+"', phone = '"+ textBox2.Text + "', email = '"+ textBox3.Text + "' where id =" + id;
            NpgsqlCommand cmd = new NpgsqlCommand(sql2, con);
            cmd.Prepare();

            cmd.ExecuteNonQuery();

            // изменение заказа


            if (checkBox1.Checked == true && checkBox2.Checked == false)
            {
                string sql3 = "update orders set ispaid = true, orderstate = 2 where num = '" + label11.Text + "'";
                NpgsqlCommand cmd3 = new NpgsqlCommand(sql3, con);
                cmd3.Prepare();

                cmd3.ExecuteNonQuery();
            }
            if (checkBox1.Checked == true && checkBox2.Checked == true)
            {
                string sql3 = "update orders set ispaid = true , isdelivered = true , orderstate = 3  where num = '" + label11.Text + "'";
                NpgsqlCommand cmd3 = new NpgsqlCommand(sql3, con);
                cmd3.Prepare();

                cmd3.ExecuteNonQuery();
            }

            // изменение товарной цпецификации


            string sql6 = "select id from orders where num = '" + label11.Text + "'";
            NpgsqlCommand com6 = new NpgsqlCommand(sql6, con);
            string idOrder = com6.ExecuteScalar().ToString();

            string sql5 = "delete from orderline where order_id = " + idOrder;
            NpgsqlCommand cmd5 = new NpgsqlCommand(sql5, con);
            cmd5.Prepare();

            cmd5.ExecuteNonQuery();

            foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    string sql4 = "INSERT INTO OrderLine(good_id, quantity, order_id) VALUES(@good_id, @quantity, @order_id)";
                    NpgsqlCommand cmd4 = new NpgsqlCommand(sql4, con);
                    cmd4.Parameters.AddWithValue("good_id", Convert.ToInt32(row.Cells[0].Value));
                    cmd4.Parameters.AddWithValue("quantity", Convert.ToInt32(row.Cells[2].Value));
                    cmd4.Parameters.AddWithValue("order_id", Convert.ToInt32(idOrder));
                    cmd4.Prepare();

                    cmd4.ExecuteNonQuery();
                }


            MessageBox.Show("Заказ №" + label11.Text + " успешно изменен.");

            Close();



        }
        // excel
        private void button1_Click(object sender, EventArgs e)
        {
            /*Excel.Application exApp = new Excel.Application();

           exApp.Workbooks.Add();
           Excel.Worksheet wsh = (Excel.Worksheet)exApp.ActiveSheet;

           int i, j;

           for(i=0;i<=dataGridView3.RowCount - 2; i++)
           {
               for (j = 0; j <= dataGridView3.ColumnCount - 1; j++)
               {
                   wsh.Cells[i+1,j+1] = dataGridView3[j,i].Value.ToString();
               }
           }


           exApp.Visible = true;*/
        }
    }
}
