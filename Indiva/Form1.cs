﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;
using System.IO;

namespace Indiva
{
    public partial class Form1 : Form {
        private DataSet tempGoodsDS = new DataSet("tempGoods");
        private DataTable tempGoodsDT = new DataTable("tempGood");
        private NpgsqlConnection con;
        private string connString = "Host=127.0.0.1;Username=postgres;Password=admin;Database=postgres";

        public Form1()
        {
            InitializeComponent();
            con = new NpgsqlConnection(connString);
            tempGoodsDS.Tables.Add(tempGoodsDT);
            con.Open();
            loadGoods();
            createTempTable();

            checkOrders();
        }

        private void createTempTable()
        {
            DataColumn idColumn = new DataColumn("Id", Type.GetType("System.Int32"));
            idColumn.Unique = true; 
            idColumn.AllowDBNull = false; 
            idColumn.AutoIncrement = true;
            idColumn.AutoIncrementSeed = 1; 
            idColumn.AutoIncrementStep = 1;

            DataColumn idGoodColumn = new DataColumn("IdGood", Type.GetType("System.Int32"));
            idGoodColumn.AllowDBNull = false;

            DataColumn nameColumn = new DataColumn("Name", Type.GetType("System.String"));
            nameColumn.AllowDBNull = false;

            DataColumn quantityColumn = new DataColumn("Quantity", Type.GetType("System.Int32"));
            nameColumn.AllowDBNull = false;

            DataColumn priceColumn = new DataColumn("Price", Type.GetType("System.Int32"));
            priceColumn.AllowDBNull = false;

            tempGoodsDT.Columns.Add(idColumn);
            tempGoodsDT.Columns.Add(idGoodColumn);
            tempGoodsDT.Columns.Add(nameColumn);
            tempGoodsDT.Columns.Add(quantityColumn);
            tempGoodsDT.Columns.Add(priceColumn);

            tempGoodsDT.PrimaryKey = new DataColumn[] { tempGoodsDT.Columns["Id"] };
        }

        private void loadGoods()
        {
            DataTable dt = new DataTable();
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter("SELECT * FROM Goods", con);
            adap.Fill(dt);
            comboBox2.DataSource = dt;
            comboBox2.DisplayMember = "name";
            comboBox2.ValueMember = "id";

            string sql = "select price from goods where id = " + this.comboBox2.SelectedValue;
            NpgsqlCommand com = new NpgsqlCommand(sql, con);
            string price = com.ExecuteScalar().ToString();
            textBox5.Text = price;

            dataGridView1.DataSource = tempGoodsDT;
        }

        private void loadClients()
        {
            DataTable dt = new DataTable();
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter("SELECT * FROM Clients", con);
            adap.Fill(dt);
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "name";
            comboBox1.ValueMember = "id";
        }

        private void addItem()
        {
            string sql = "select price from goods where id = "+ this.comboBox2.SelectedValue;
            NpgsqlCommand com = new NpgsqlCommand(sql,con);
            string price = com.ExecuteScalar().ToString();
            DataRow row = tempGoodsDT.NewRow();
            row.ItemArray = new object[] { null, Convert.ToInt32(this.comboBox2.SelectedValue), this.comboBox2.GetItemText(this.comboBox2.SelectedItem), Convert.ToInt32(this.textBox4.Text),Convert.ToInt32(price)* Convert.ToInt32(this.textBox4.Text)};
            tempGoodsDT.Rows.Add(row); 
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            con.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                loadClients();
            }
            else
            {
                comboBox1.DataSource = null;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (comboBox2.SelectedValue.ToString() != "System.Data.DataRowView")
            {
                string sql = "select price from goods where id = " + this.comboBox2.SelectedValue;
                NpgsqlCommand com = new NpgsqlCommand(sql, con);
                string price = com.ExecuteScalar().ToString();
                textBox5.Text = price;
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(textBox4.Text))
            {
                MessageBox.Show("Введите кол-во товара!");

            }
            else
            {
                if (Convert.ToInt32(this.textBox4.Text) > 0)
                {


                    // добавление в грид
                    addItem();


                }
                else
                {
                    MessageBox.Show("Введите кол-во товара > 0!");
                }

            }

            updateTotal();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.dataGridView1.SelectedRows)
            {
                dataGridView1.Rows.RemoveAt(item.Index);
            }
            updateTotal();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            string sql = "INSERT INTO clients(name, phone, email) VALUES(@name, @phone, @email)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
            cmd.Parameters.AddWithValue("name",this.textBox1.Text);
            cmd.Parameters.AddWithValue("phone", this.textBox2.Text);
            cmd.Parameters.AddWithValue("email", this.textBox3.Text);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
            this.textBox1.Text = "";
            this.textBox2.Text = "";
            this.textBox3.Text = "";
            checkBox1.Checked = true;
        }

        private void updateTotal()
        {
            int totalQuantity = 0;
            int totalPrice = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows){
                totalQuantity += Convert.ToInt32(row.Cells[3].Value);
                totalPrice += Convert.ToInt32(row.Cells[4].Value);
            }
            label9.Text = totalQuantity.ToString();
            label11.Text = totalPrice.ToString();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string sql = "INSERT INTO orders(num, client_id, isPaid, orderState, dateCreate, dateToPay) VALUES(@num, @client_id, @isPaid, @orderState, @dateCreate, @dateToPay)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
            DateTime cancelDate = DateTime.Now.AddDays(20);
            Guid uuid = Guid.NewGuid();
            cmd.Parameters.AddWithValue("num", uuid);
            cmd.Parameters.AddWithValue("client_id", Convert.ToInt32(this.comboBox1.SelectedValue));
            cmd.Parameters.AddWithValue("isPaid", false);
            cmd.Parameters.AddWithValue("orderState", 1);
            cmd.Parameters.AddWithValue("dateCreate", OdbcType.Timestamp).Value = DateTime.Now;
            cmd.Parameters.AddWithValue("dateToPay", OdbcType.Timestamp).Value = DateTime.Now.AddDays(20);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
            addItemsInOrder(uuid, cancelDate);

        }

        private void addItemsInOrder(Guid uuid, DateTime cancelDate)
        {
            string sql1 = "select id from orders where num = '" + uuid+"'";
            NpgsqlCommand com = new NpgsqlCommand(sql1, con);
            string id = com.ExecuteScalar().ToString();
            while (dataGridView1.Rows.Count > 0)
            {
                string sql = "INSERT INTO OrderLine(good_id, quantity, order_id) VALUES(@good_id, @quantity, @order_id)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("good_id", Convert.ToInt32(dataGridView1.Rows[0].Cells[1].Value));
                cmd.Parameters.AddWithValue("quantity", Convert.ToInt32(dataGridView1.Rows[0].Cells[3].Value));
                cmd.Parameters.AddWithValue("order_id", Convert.ToInt32(id));
                cmd.Prepare();

                cmd.ExecuteNonQuery();
                dataGridView1.Rows.Remove(dataGridView1.Rows[0]);
            }
            updateTotal();
            MessageBox.Show("Заказ №"+uuid+" успешно создан. Нужно платить до "+cancelDate.Date+", иначе автоматически отменится!");
        }

        private void loadOrders()
        {
            

            DataTable dt = new DataTable();
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter("SELECT num FROM orders where orderstate = 1 order by datecreate desc", con);
            adap.Fill(dt);
            dataGridView2.DataSource = dt;

            DataTable dt2 = new DataTable();
            NpgsqlDataAdapter adap2 = new NpgsqlDataAdapter("SELECT num FROM orders where orderstate = 2 order by datecreate desc", con);
            adap2.Fill(dt2);
            dataGridView3.DataSource = dt2;

            DataTable dt3 = new DataTable();
            NpgsqlDataAdapter adap3 = new NpgsqlDataAdapter("SELECT num FROM orders where orderstate = 3 order by datecreate desc", con);
            adap3.Fill(dt3);
            dataGridView4.DataSource = dt3;

            DataTable dt4 = new DataTable();
            NpgsqlDataAdapter adap4 = new NpgsqlDataAdapter("SELECT num FROM orders where orderstate = 4 or orderstate = 5 order by datecreate desc", con);
            adap4.Fill(dt4);
            dataGridView5.DataSource = dt4;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 1)
            {
                loadOrders();
                
            }
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {


            loadOrderData( dataGridView2);
              


        }

        private void dataGridView3_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {


            loadOrderData(  dataGridView3);


        }

        private void dataGridView4_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {


            loadOrderData( dataGridView4);

        }

        private void dataGridView5_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            

            loadOrderData( dataGridView5);

           
        }

        private void checkOrders()
        {
            //проверка заказов на опалту 20 дней
            Dictionary<int, DateTime> orders = new Dictionary<int, DateTime>();
            string sql = "select id, dateToPay from orders";
            NpgsqlCommand com = new NpgsqlCommand(sql, con);
            NpgsqlDataReader rdr = com.ExecuteReader();
            while (rdr.Read())
            {
                orders.Add(rdr.GetInt32(0), rdr.GetDateTime(1));
            }
            rdr.Close();
            foreach (KeyValuePair<int, DateTime> i in orders)
            {
                if (i.Value < DateTime.Now)
                {
                    string sql2 = "update orders set orderState = 4 where ispaid = false and id ="+i.Key.ToString();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql2, con);
                    cmd.Prepare();

                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void loadOrderData(DataGridView grid)
        {
          

            OrderForm form = new OrderForm(); 

            form.label11.Text = grid.CurrentRow.Cells[0].Value.ToString();

            // инфа по заказу
            DataTable dt = new DataTable();
            NpgsqlDataAdapter adap = new NpgsqlDataAdapter("SELECT * FROM orders where num ='" + grid.CurrentRow.Cells[0].Value.ToString() + "'", con);
            adap.Fill(dt);

            form.label6.Text = dt.Rows[0][6].ToString();
            form.label7.Text = dt.Rows[0][7].ToString();
            form.checkBox1.Checked = Convert.ToBoolean(dt.Rows[0][3]);
            form.checkBox2.Checked = Convert.ToBoolean(dt.Rows[0][4]);

            //статус заказа
            string sql1 = "select name from OrderState where id = " + dt.Rows[0][5].ToString();
            NpgsqlCommand com = new NpgsqlCommand(sql1, con);
            string state = com.ExecuteScalar().ToString();

            form.label8.Text = state;

            // инфа по клиенту
            DataTable dt2 = new DataTable();
            NpgsqlDataAdapter adap2 = new NpgsqlDataAdapter("SELECT * FROM clients where id =" + dt.Rows[0][2].ToString(), con);
            adap2.Fill(dt2);

            form.textBox1.Text = dt2.Rows[0][1].ToString();
            form.textBox2.Text = dt2.Rows[0][2].ToString();
            form.textBox3.Text = dt2.Rows[0][3].ToString();

            // инфа по товарам
            DataTable dt3 = new DataTable();
            NpgsqlDataAdapter adap3 = new NpgsqlDataAdapter("select g.id, g.name, o.quantity, g.price from orderline o left join goods g on o.good_id = g.id where order_id = " + dt.Rows[0][0].ToString(), con);
            adap3.Fill(dt3);

            form.dataGridView1.DataSource = dt3;

            // проверка активности кнопок

            if (state != "Ждет оплату" && state != "Доставляется")
            {
                form.button2.Enabled = false;
                form.button4.Enabled = false;
            }

            if (state != "Ждет оплату" )
            {

                form.button3.Enabled = false;

            }
            

            form.ShowDialog();



        }

        private void changeStatusButton_Click(object sender, EventArgs e)
        {
            DateTime fromDate = fromDateTimePicker.Value;
            DateTime toDate = toDateTimePicker.Value;
            string sql = "select ord.id as Идентификатор, ord.num as Номер, os.name as Статус ,ord.datecreate as Дата , c.name as ФИО, c.phone as Телефон, c.email as Email, g.name as  Товар, ol.quantity as Количество, g.price as Цена, ol.quantity * g.price as Сумма from orders ord left join clients c on c.id = ord.client_id left join orderline ol on ol.order_id = ord.id left join goods g on g.id = ol.good_id left join orderstate os on ord.orderstate = os.id where ord.orderstate = 3 and ord.datecreate  >= '" + fromDate+"' AND ord.datecreate <= '"+toDate+"' order by ord.datecreate desc";
            NpgsqlCommand command = new NpgsqlCommand(sql, con);
            NpgsqlDataReader reader = command.ExecuteReader();

            var csv = new StringBuilder();
            /*var newLine = string.Format("{0}", "sep=;");
            csv.AppendLine(newLine);*/

            var newLine = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}", "Идентификатор", "Номер заказа", "Статус заказа", "Дата создания", "ФИО клиента", "Номер телефона клиента", "Email клиента", "Наименование товара", "Кол-во", "Цена", "Сумма");
            csv.AppendLine(newLine);

            while (reader.Read())
            {
                var id = reader[0].ToString();
                var orderNum = reader[1].ToString();
                var state = reader[2].ToString();
                var dateCreate = Convert.ToDateTime(reader[3]).ToString();
                var name = reader[4].ToString();
                var phone = reader[5].ToString();
                var email = reader[6].ToString();
                var good = reader[7].ToString();
                var quantity = reader[8].ToString();
                var price = reader[9].ToString();
                var sum = reader[10].ToString();

                newLine = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}", id, orderNum, state,dateCreate, name, phone, email, good, quantity, price, sum);
                csv.AppendLine(newLine);
            }


            File.WriteAllText("Y:\\Desktop\\ᴜɴɪ\\C#\\CSharpPostgreSQL\\result.csv", csv.ToString());
        
            reader.Close();

            MessageBox.Show("Отчет сформирован.");
        }
    }
}
