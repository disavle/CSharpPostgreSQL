--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

-- Started on 2022-05-03 16:54:38

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 3383 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 3 (class 3079 OID 16468)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 3384 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 214 (class 1259 OID 16421)
-- Name: clients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clients (
    id integer NOT NULL,
    name character varying(300) NOT NULL,
    phone character varying(12) NOT NULL,
    email character varying(300) NOT NULL
);


ALTER TABLE public.clients OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16420)
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clients_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clients_id_seq OWNER TO postgres;

--
-- TOC entry 3385 (class 0 OID 0)
-- Dependencies: 213
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clients_id_seq OWNED BY public.clients.id;


--
-- TOC entry 212 (class 1259 OID 16395)
-- Name: goods; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.goods (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    price integer NOT NULL
);


ALTER TABLE public.goods OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16394)
-- Name: goods_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.goods_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_id_seq OWNER TO postgres;

--
-- TOC entry 3386 (class 0 OID 0)
-- Dependencies: 211
-- Name: goods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.goods_id_seq OWNED BY public.goods.id;


--
-- TOC entry 220 (class 1259 OID 16677)
-- Name: orderline; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orderline (
    id integer NOT NULL,
    good_id integer NOT NULL,
    quantity integer NOT NULL,
    order_id integer NOT NULL
);


ALTER TABLE public.orderline OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16676)
-- Name: orderline_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orderline_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orderline_id_seq OWNER TO postgres;

--
-- TOC entry 3387 (class 0 OID 0)
-- Dependencies: 219
-- Name: orderline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orderline_id_seq OWNED BY public.orderline.id;


--
-- TOC entry 218 (class 1259 OID 16656)
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders (
    id integer NOT NULL,
    num uuid NOT NULL,
    client_id integer NOT NULL,
    ispaid boolean DEFAULT false,
    isdelivered boolean DEFAULT false,
    orderstate integer NOT NULL,
    datecreate timestamp without time zone NOT NULL,
    datetopay timestamp without time zone NOT NULL
);


ALTER TABLE public.orders OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16655)
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_id_seq OWNER TO postgres;

--
-- TOC entry 3388 (class 0 OID 0)
-- Dependencies: 217
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- TOC entry 216 (class 1259 OID 16497)
-- Name: orderstate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orderstate (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.orderstate OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16496)
-- Name: orderstate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orderstate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orderstate_id_seq OWNER TO postgres;

--
-- TOC entry 3389 (class 0 OID 0)
-- Dependencies: 215
-- Name: orderstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orderstate_id_seq OWNED BY public.orderstate.id;


--
-- TOC entry 3197 (class 2604 OID 16424)
-- Name: clients id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients ALTER COLUMN id SET DEFAULT nextval('public.clients_id_seq'::regclass);


--
-- TOC entry 3196 (class 2604 OID 16398)
-- Name: goods id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.goods ALTER COLUMN id SET DEFAULT nextval('public.goods_id_seq'::regclass);


--
-- TOC entry 3202 (class 2604 OID 16680)
-- Name: orderline id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orderline ALTER COLUMN id SET DEFAULT nextval('public.orderline_id_seq'::regclass);


--
-- TOC entry 3199 (class 2604 OID 16659)
-- Name: orders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- TOC entry 3198 (class 2604 OID 16500)
-- Name: orderstate id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orderstate ALTER COLUMN id SET DEFAULT nextval('public.orderstate_id_seq'::regclass);


--
-- TOC entry 3371 (class 0 OID 16421)
-- Dependencies: 214
-- Data for Name: clients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clients (id, name, phone, email) FROM stdin;
2	Петров Петр Петрович	88005353556	petr@mail.ru
4	Дима	790966	email@mail.com
1	Иванов Иван Ивановичfd	88005353524	ivan@mail.ruds
\.


--
-- TOC entry 3369 (class 0 OID 16395)
-- Dependencies: 212
-- Data for Name: goods; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.goods (id, name, price) FROM stdin;
1	Теннисный мяч	500
2	Куртка BatNorton мужская	10000
3	Куртка BatNorton женская	10000
4	Штаны спортивные	5000
6	Штаны спортивные Demix	4000
\.


--
-- TOC entry 3377 (class 0 OID 16677)
-- Dependencies: 220
-- Data for Name: orderline; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orderline (id, good_id, quantity, order_id) FROM stdin;
1	2	2	1
83956	3	2	2
5	1	2	5
6	2	1	5
7	1	1	6
8	3	2	7
83962	6	3	8
83971	1	1	9
\.


--
-- TOC entry 3375 (class 0 OID 16656)
-- Dependencies: 218
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orders (id, num, client_id, ispaid, isdelivered, orderstate, datecreate, datetopay) FROM stdin;
8	66904bbd-8985-4d8e-b3a0-9a9ed76cfa73	4	f	f	1	2022-05-03 16:31:08.659563	2022-05-23 16:31:08.659563
6	3cd3217a-8ad6-4d36-9983-b546c0b6cfe1	2	t	t	3	2022-05-03 16:22:42.433547	2022-05-23 16:22:42.433547
7	78eb2e1e-6b86-46c9-80b5-5bae967a6b76	2	t	f	2	2022-05-03 16:22:55.203591	2022-05-23 16:22:55.203591
2	5cb856c8-3c0d-4994-8e21-2355fdaa6adc	2	f	f	4	2022-04-12 10:50:33.742909	2022-04-13 10:50:33.742909
9	218fb0b4-d3f0-4a2b-bcc0-02ed1f5b599a	1	f	f	1	2022-05-03 16:48:47.302856	2022-05-23 16:48:47.302856
1	b47c5f45-a061-4e26-b729-a2c2dfd5ced8	1	f	f	5	2022-05-03 10:49:46.634994	2022-05-23 10:49:46.634994
5	32aaa697-4df3-474e-8ea9-01f4d3b0aef1	1	f	f	5	2022-05-03 15:01:19.730133	2022-05-23 15:01:19.730133
\.


--
-- TOC entry 3373 (class 0 OID 16497)
-- Dependencies: 216
-- Data for Name: orderstate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orderstate (id, name) FROM stdin;
1	Ждет оплату
2	Доставляется
3	Доставлен
4	Просрочен
5	Отменен
\.


--
-- TOC entry 3390 (class 0 OID 0)
-- Dependencies: 213
-- Name: clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clients_id_seq', 4, true);


--
-- TOC entry 3391 (class 0 OID 0)
-- Dependencies: 211
-- Name: goods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.goods_id_seq', 6, true);


--
-- TOC entry 3392 (class 0 OID 0)
-- Dependencies: 219
-- Name: orderline_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orderline_id_seq', 83971, true);


--
-- TOC entry 3393 (class 0 OID 0)
-- Dependencies: 217
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_id_seq', 9, true);


--
-- TOC entry 3394 (class 0 OID 0)
-- Dependencies: 215
-- Name: orderstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orderstate_id_seq', 5, true);


--
-- TOC entry 3208 (class 2606 OID 16434)
-- Name: clients clients_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_email_key UNIQUE (email);


--
-- TOC entry 3210 (class 2606 OID 16430)
-- Name: clients clients_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_name_key UNIQUE (name);


--
-- TOC entry 3212 (class 2606 OID 16432)
-- Name: clients clients_phone_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_phone_key UNIQUE (phone);


--
-- TOC entry 3214 (class 2606 OID 16428)
-- Name: clients clients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- TOC entry 3204 (class 2606 OID 16402)
-- Name: goods goods_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.goods
    ADD CONSTRAINT goods_name_key UNIQUE (name);


--
-- TOC entry 3206 (class 2606 OID 16400)
-- Name: goods goods_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.goods
    ADD CONSTRAINT goods_pkey PRIMARY KEY (id);


--
-- TOC entry 3224 (class 2606 OID 16682)
-- Name: orderline orderline_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orderline
    ADD CONSTRAINT orderline_pkey PRIMARY KEY (id);


--
-- TOC entry 3220 (class 2606 OID 16665)
-- Name: orders orders_num_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_num_key UNIQUE (num);


--
-- TOC entry 3222 (class 2606 OID 16663)
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- TOC entry 3216 (class 2606 OID 16504)
-- Name: orderstate orderstate_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orderstate
    ADD CONSTRAINT orderstate_name_key UNIQUE (name);


--
-- TOC entry 3218 (class 2606 OID 16502)
-- Name: orderstate orderstate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orderstate
    ADD CONSTRAINT orderstate_pkey PRIMARY KEY (id);


--
-- TOC entry 3225 (class 2606 OID 16666)
-- Name: orders fk_client; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT fk_client FOREIGN KEY (client_id) REFERENCES public.clients(id);


--
-- TOC entry 3227 (class 2606 OID 16683)
-- Name: orderline fk_good; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orderline
    ADD CONSTRAINT fk_good FOREIGN KEY (good_id) REFERENCES public.goods(id);


--
-- TOC entry 3228 (class 2606 OID 16688)
-- Name: orderline fk_order; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orderline
    ADD CONSTRAINT fk_order FOREIGN KEY (order_id) REFERENCES public.orders(id);


--
-- TOC entry 3226 (class 2606 OID 16671)
-- Name: orders fk_state; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT fk_state FOREIGN KEY (orderstate) REFERENCES public.orderstate(id);


-- Completed on 2022-05-03 16:54:39

--
-- PostgreSQL database dump complete
--

